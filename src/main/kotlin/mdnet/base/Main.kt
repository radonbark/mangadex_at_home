/*
Mangadex@Home
Copyright (c) 2020, MangaDex Network
This file is part of MangaDex@Home.

MangaDex@Home is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MangaDex@Home is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this MangaDex@Home.  If not, see <http://www.gnu.org/licenses/>.
 */
package mdnet.base

import ch.qos.logback.classic.LoggerContext
import kotlin.system.exitProcess
import mdnet.BuildInfo
import org.slf4j.LoggerFactory

object Main {
    private val LOGGER = LoggerFactory.getLogger(Main::class.java)

    @JvmStatic
    fun main(args: Array<String>) {
        println(
            "Mangadex@Home Client Version ${BuildInfo.VERSION} (Build ${Constants.CLIENT_BUILD}) initializing"
        )
        println()
        println("Copyright (c) 2020, MangaDex Network")
        println("""
            Mangadex@Home is free software: you can redistribute it and/or modify
            it under the terms of the GNU General Public License as published by
            the Free Software Foundation, either version 3 of the License, or
            (at your option) any later version.

            Mangadex@Home is distributed in the hope that it will be useful,
            but WITHOUT ANY WARRANTY; without even the implied warranty of
            MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
            GNU General Public License for more details.

            You should have received a copy of the GNU General Public License
            along with Mangadex@Home.  If not, see <https://www.gnu.org/licenses/>.
        """.trimIndent())

        var file = "settings.json"
        if (args.size == 1) {
            file = args[0]
        } else if (args.isNotEmpty()) {
            dieWithError("Expected one argument: path to config file, or nothing")
        }

        val client = MangaDexClient(file)
        Runtime.getRuntime().addShutdownHook(Thread { client.shutdown() })
        client.runLoop()
    }

    fun dieWithError(e: Throwable): Nothing {
            LOGGER.error(e) { "Critical Error" }
        (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        exitProcess(1)
    }

    fun dieWithError(error: String): Nothing {
        LOGGER.error { "Critical Error: $error" }

        (LoggerFactory.getILoggerFactory() as LoggerContext).stop()
        exitProcess(1)
    }
}
